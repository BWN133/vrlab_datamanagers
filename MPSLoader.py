from scipy.io import loadmat
import numpy as np
import csv
import os
# Description:
# Data loader that are capable of transforming MeshPuipleShadow matlab file into ShadowGaze.csv (skeleton data and gaze data)
# and into RotationalMatrix.csv (rotation matrix for aligning meshes)
class MPSLoader:
    p = ""
    root = ""
    data = []
    header = []
    def __init__(self, path="."):
        self.p = os.path.join(path, "pupilShadowMesh.mat")
        self.root = path

    def getPath(self):
        return self.p
    
    def loadMAT(self):
        self.data = loadmat(self.p)
        
    def data_type_transformer(self, inputN):
        result = []
        for i in range(len(inputN)):
            if i == 0:
                result.append(int(inputN[i]))
            else:
                result.append(inputN[i])
        return result

    def makeCSV(self, input, output_header,csvName="data.csv"):
        csvfile = os.path.join(self.root, csvName)
        with open(csvfile, 'w', encoding='UTF8') as f:
            writer = csv.writer(f)
            if(output_header is not None):
                writer.writerow(output_header)
            # write the data
            for i in input:
                row = i
                if(output_header is not None):
                    row = self.data_type_transformer(i)
                writer.writerow(row)

    def create_header(self):
        
        input_header = []
        output_header= ['timeStamp']
        trackername = ['gaze']

        for i in range(30):
            if i == 26:
                trackername.append('head')
            else:
                trackername.append('a')
        for name in trackername:
            input_header.append(name)
            for c in ['x','y','z']:
                output_header.append(name + '_position_'+c)
        self.header = output_header
        

    def process(self):
        self.loadMAT()
        self.create_header()
        assert len(self.data) > 0
        assert len(self.header) == 94
        ### Load in Shadow and gaze i.e. Skeleton data and the position that the subject gaze at to csv
        sdata = self.data["shadow"]
        gdata = self.data["gazeXYZ"]
        rdata = self.data["orig2alignedMat"]
        skresult = []
        for timeStamp_index in range(sdata.shape[0]):
            row = [timeStamp_index]
            # Load in gaze data of current time stamp
            for i in range(3):
                row.append(gdata[timeStamp_index][i])
            # Load in skeleton data of current time stamp
            for device_index in range(sdata.shape[1]):
                cur_device = sdata[timeStamp_index][device_index]
                for i in range(sdata.shape[2]):
                    row.append(cur_device[i])
            skresult.append(row)
        self.makeCSV(skresult, self.header, csvName = "ShadowGaze.csv")
        self.makeCSV(rdata, None, csvName = "RotationMatrix.csv")
        
