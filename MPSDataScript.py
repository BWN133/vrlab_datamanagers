import argparse
import os
from MPSLoader import MPSLoader

FILENAME = "pupilShadowMesh.mat"
parser = argparse.ArgumentParser(description='Acceptfiles')
parser.add_argument('-d', dest="directory", default=".", help='an integer for the accumulator')
# parser.add_argument('--sum', dest='accumulate', action='store_const',const=sum, default=max, help='sum the integers (default: find the max)')
args = parser.parse_args()

directory = args.directory
# iterate over files in
# that directory
for root, dirs, files in os.walk(directory):
    for filename in files:
        if filename == FILENAME:
            mpsloader = MPSLoader(root)
            mpsloader.process()
